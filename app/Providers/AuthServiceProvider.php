<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
   
    protected $policies = [
        'App\Task' => 'App\Policies\TaskPolicy',
    ];

    public function boot()
    {
        $this->registerPolicies();

        // cd x10000 >:c
    }
}
